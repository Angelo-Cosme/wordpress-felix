<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @since Felix
 */
?>


<section id="footer">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-12">
            <div class="footer-top">
              <div class="row align-items-center justify-content-between">
                <div class="col-md-5">
                  <a href="#" class="logo-buttom float-start">Felix</a>
                </div>
                <div class="col-md-7">
                  <ul>
                    <li><a href="#">Accueil</a></li>
                    <li><a href="#about">A Propos</a></li>
                    <li><a href="#services">Services</a></li>
                    <li><a href="<?php echo esc_url( home_url( '/besoin de devis/' ) ); ?>">Contact</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="footer-bottom">
              <div class="row align-items-center justify-content-between">
                <div class="col-md-12">
                  <p class="text-center">© 2022 Tous Droits Réservés | Felix</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

	
</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>
