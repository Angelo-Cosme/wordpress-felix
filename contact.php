<?php
/**
 * Template name: Besoin de devis
 *
 * @package WordPress
 * @since Felix
 */

get_header(); ?>

  <section id="wrapper-contact" class="py-11">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-11 col-xl-8">
          <h1><?php _e('Expliquez-nous votre projet et recevez un devis en 24h', 'felix'); ?></h1>
        </div>
      </div>
    </div>
  </section>

  <section id="wrap-contact" class="py-11">
    <div class="container">
      <div class="row">
        <div class="col-lg-4">
          <div class="info-box">
            <h4 class="mb-5"><?php _e('Contact Support', 'felix'); ?></h4>
            <div class="row">
              <div class="col-12">
                <ul>
                  <li class="d-flex py-2">
                    <div class="px-2">
                      <h3><?php _e('Téléphone', 'felix'); ?></h3>
                      <p><?php _e('+229 96 81 41 42', 'felix'); ?></p>
                    </div>
                  </li>
        
                  <li class="d-flex py-2">
                    <div class="px-2">
                      <h3><?php _e('Email', 'felix'); ?></h3>
                      <p><?php _e('contact@noumiton.com', 'felix'); ?></p>
                      <p><?php _e('info@noumiton.com', 'felix'); ?></p>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="offset-1 col-lg-7">
          <h2 class="mb-5"><?php _e('Ecrivez-nous par ce formulaire', 'felix'); ?></h2>
          <form action="#" method="POST" class="formular">
            <?php the_content(); ?>
          </form>
          
        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>