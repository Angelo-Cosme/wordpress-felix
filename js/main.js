
/*****************************
          Partners
 ****************************/
    var $ = jQuery;
    jQuery(document).ready(function($){
      $('.partners-wrap').slick({
        dots: false,
        infinite: true,
        autoplaySpeed: 1000,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        arrow: false,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 1, 
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          }
        ]
      });
    });