<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @since Felix 0.1
 */

get_header(); 

$description_projet = get_field("description-projet");

?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content col-lg-7 offset-3 projet-single" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>

					<?php if ( '' != $description_projet ) { ?>
						<h5><?php echo $description_projet; ?></h5>
					<?php } ?>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->


<?php get_footer(); ?>