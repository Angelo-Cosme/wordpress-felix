<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 * 
 * 
 * Template name: FELIX ENTREPRISE FR
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @since Felix
 */

get_header(); 

  $mission = get_field("mission");
  $subtitlemission = get_field("subtitlemission");
  $paramission = get_field("paramission");
  $customertitle = get_field("customertitle");
  $paracustomer = get_field("paracustomer");
  $product = get_field("product");
  $paraproduct = get_field("paraproduct");
  $service = get_field("service");
  $paraservice = get_field("paraservice");
  $imgabout = get_field("imgabout");
  $picture = $imgabout['sizes']['large'];
  $readmore = get_field('readmore');

  $experiences = get_field('experiences');
  $projets = get_field('projets');
  $satisfy = get_field('satisfy');

  $title_service = get_field("title-service");
  $web_design_title = get_field("web-design-title");
  $web_design_content = get_field("web-design-content");
  $developpment_title = get_field("developpment-title");
  $developpment_content = get_field("developpment-content");
  $referencement_title = get_field("referencement-title");
  $referencement_content  = get_field("referencement-content");
  $btn_service = get_field('btn-service');




?> 



  <section id="wrapper-slide" class="py-11">
    <video autoplay muted loop class="myVideo">
      <source src="<?php echo get_template_directory_uri(); ?>/images/gif-slide-felix.mp4" type="video/mp4">
    </video>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-11 col-xl-8">
          
        </div>
      </div>
    </div>
  </section>

  <section id="about" class="py-11">
      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-lg-4 col-md-9 col-xs-11">

            
          
          <?php if ( '' != $mission ) { ?>
            <h5><?php echo $mission; ?></h5>
          <?php } ?>

          <?php if ( '' != $subtitlemission ) { ?>
            <h2><?php echo $subtitlemission; ?></h2>
          <?php } ?>

          <?php if ( '' != $paramission ) { ?>
            <p><?php echo $paramission; ?></p>
          <?php } ?>

            <a href="<?php echo $readmore ?>" class="btn btn-abt">En savoir plus</a>
          </div>

          <div class="col-lg-4 col-md-6">
            <img src="<?php echo $picture; ?>" class="img-fluid" >
          </div>
          <div class="col-lg-4 col-md-6 col-xs-3 order-3">
            <div class="widget-info">
              <?php if ( '' != $customertitle ) { ?>
                <h5><?php echo $customertitle; ?></h5>
              <?php } ?>

              <?php if ( '' != $paracustomer ) { ?>
                <p><?php echo $paracustomer; ?></p>
              <?php } ?>

            </div>
            <div class="widget-info">
              <?php if ( '' != $product ) { ?>
                <h5><?php echo $product; ?></h5>
              <?php } ?>

              <?php if ( '' != $paraproduct ) { ?>
                <p><?php echo $paraproduct; ?></p>
              <?php } ?>

            </div>
            <div class="widget-info">
              <?php if ( '' != $service ) { ?>
                <h5><?php echo $service; ?></h5>
              <?php } ?>
              <?php if ( '' != $paraservice ) { ?>
                <p><?php echo $paraservice; ?></p>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="infos" class="py-11">
      <div class="container">
        <div class="row m-auto align-items-center justify-content-center">
          <div class="col-md-4">
            <img src="<?php echo get_template_directory_uri(); ?>/images/code.png" class="img-fluid" alt="felix-row">
              <?php if ($experiences): ?>
                <span><?php echo $experiences; ?></span>
              <?php endif ?>
            <p><?php _e('Années d\'expériences', 'felix'); ?></p>
          </div>
          <div class="col-md-4">
            <img src="<?php echo get_template_directory_uri(); ?>/images/projets.png" class="img-fluid" alt="felix-row">
              <?php if ($projets): ?>
                <span><?php echo $projets; ?></span>
              <?php endif ?>
            <p><?php _e('Projets Web Terminés', 'felix'); ?></p>
          </div>
          <div class="col-md-4">
            <img src="<?php echo get_template_directory_uri(); ?>/images/customer-satisfaction.png" class="img-fluid" alt="felix-row">
              <?php if ($satisfy): ?>
                <span><?php echo $satisfy; ?></span>
              <?php endif ?>
            <p><?php _e('Clients Satisfaits', 'felix'); ?></p>
          </div>
        </div>
      </div>
    </section>

    <section id="services" class="py-11">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 col-md-12">
            <h5><?php _e('Découvrez nos services', 'felix'); ?></h5>
              <?php if ($title_service): ?>
                <h2><?php echo $title_service ?></h2>
              <?php endif ?>
            <a href="<?php echo $btn_service; ?>" class="btn btn-svc mt-11">Contactez-nous</a>
          </div>
          <div class="col-lg-7 col-md-12">
            <p><?php _e('Avec plus 14 ans d\'expériences nous avons conquéris le coeur de tout ceux et celles qui nous confiancés leur projet. Nos services aux valeurs ajoutées qui font notre difference.', 'felix'); ?></p>
            <div class="row service-widget">
              <div class="col-2 img-service"><img src="<?php echo get_template_directory_uri(); ?>/images/services-icon-1.png" class="img-fluid mt-4" alt="img-service"></div>
              <div class="col-10 text-service">
                <?php if ($web_design_title): ?>
                  <h3><?php echo $web_design_title ?></h3>
                <?php endif ?>
                <?php if ($web_design_content): ?>
                  <p><?php echo $web_design_content ?></p>
                <?php endif ?>
              </div>
            </div>
            <div class="row service-widget">
              <div class="col-2 img-service"><img src="<?php echo get_template_directory_uri(); ?>/images/services-icon-2.png" class="img-fluid mt-4" alt="img-service"></div>
              <div class="col-10 text-service">
                <?php if ($developpment_title): ?>
                  <h3><?php echo $developpment_title ?></h3>
                <?php endif ?>
                <?php if ($developpment_content): ?>
                  <p><?php echo $developpment_content ?></p>
                <?php endif ?>
              </div>
            </div>
            <div class="row service-widget">
              <div class="col-2 img-service"><img src="<?php echo get_template_directory_uri(); ?>/images/services-icon-3.png" class="img-fluid mt-4" alt="img-service"></div>
              <div class="col-10 text-service">
                <?php if ($referencement_title): ?>
                  <h3><?php echo $referencement_title ?></h3>
                <?php endif ?>
                <?php if ($referencement_content): ?>
                  <p><?php echo $referencement_content ?></p>
                <?php endif ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="bannerdev" class="py-11"></section>

  
    <section id="portfolio">

      


      <div class="container py-11">
        <h2 class="text-center"><?php _e('Notre Portfolio', 'felix'); ?></h2>
        <div class="row">
        <div class="text-center mb-5">
          <a class="btn btn-default filter-button" data-filter="all"><?php _e('Tous les travaux', 'felix'); ?></a>
          
          <?php
            $terms = get_terms('porfiolio_category');
            foreach ($terms as $term) { ?>
            <a class="btn btn-default filter-button" data-filter="<?php echo $term->slug; ?>"><?php echo $term->name; ?></a>
          <?php }

          ?>
          
        </div>
          
          <?php

            $args = array (
              'post_type' => 'portfolio',
              'post_per_page' => 8
            );

            $query = new WP_Query($args);

            while ($query->have_posts()){
              $query->the_post(); 
              
              $termsArray = get_the_terms($post->ID, 'porfiolio_category');
              
              $termsSLug = "";
              if (is_array($termsArray) || is_object($termsArray)) {
                foreach ($termsArray as $term) {
                  $termsSLug .= $term->slug . ' ';
                }
              }

              ?>
            

            <div class="portfolio col-lg-4 col-md-4 col-sm-4 col-xs-6 filter <?php echo $termsSLug ?>">
              <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid"></a>
            </div>

            <?php }

             wp_reset_postdata();

          ?>



          

        </div>
      </div>
    </section>
  

<?php get_footer(); ?>